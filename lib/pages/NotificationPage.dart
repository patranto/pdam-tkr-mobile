import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class NotificationPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: _head(context),
      body: _body(context),
    );
  }

  Widget _head(BuildContext context) {
    return AppBar(
      title: Text("Notifications"),
    );
  }

  Widget _body(BuildContext context) {
    return ListView(
      children: <Widget>[

        createTile("Notifications 1", Ionicons.ios_water),
        createTile("Notifications 2", Ionicons.ios_water),
        createTile("Notifications 3", Ionicons.ios_water),
        createTile("Notifications 4", Ionicons.ios_water),
        createTile("Notifications 5", Ionicons.ios_water),


      ],
    );
  }

  ListTile createTile(String title, IconData icon) {
    return ListTile(
      title: Text(title),
      subtitle: Text('Sub Information'),
      trailing: Icon(Ionicons.ios_notifications),
    );
  }

}