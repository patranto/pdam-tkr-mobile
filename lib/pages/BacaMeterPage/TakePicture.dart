import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class TakePicture extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TakePictureState();
  }

} 

class _TakePictureState extends State<TakePicture> {

  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {

    var center = Center(
      child: Icon(
        Icons.camera,
        color: Colors.black38,
        size: 42,
      ),
    );

    var imageCamera = Column(
      children: <Widget>[
        Expanded(
          child: _image != null ? Image.file(_image):null,
        )
      ],
    );

    return GestureDetector(
      onTap: () => {
        getImage()
      },
      child: Container(
        width: double.maxFinite,
        height: 200,
        decoration: BoxDecoration(
          color: Colors.white24,
          borderRadius: BorderRadius.all(Radius.circular(4)),
          border: Border.all(
            color: Colors.black26,
            width: 1,
          )
        ),
        child: _image == null ? center:imageCamera,
      ),
    );
  }

}