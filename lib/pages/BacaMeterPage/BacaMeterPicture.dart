import 'package:flutter/material.dart';

import 'TakePicture.dart';

class BacaMeterPicture extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BacaMeterPictureState();
  }
  
}

class _BacaMeterPictureState extends State<BacaMeterPicture> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          TakePicture(),
          SizedBox(height: 12),
          Form(
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Masukan angka meteran'
                  ),
                ),
                SizedBox(height: 30),
                RaisedButton(
                  onPressed: () => {},
                  child: Text('SIMPAN'),
                )
              ],
            )
          )
        ],
      ),
    );
  }

}