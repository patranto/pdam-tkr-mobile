import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class BacaMeterHistory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[

        createTile("History 1", Ionicons.ios_water),
        createTile("History 2", Ionicons.ios_water),
        createTile("History 3", Ionicons.ios_water),
        createTile("History 4", Ionicons.ios_water),
        createTile("History 5", Ionicons.ios_water),


      ],
    );
  }

  ListTile createTile(String title, IconData icon) {
    return ListTile(
      title: Text(title),
      leading: Icon(icon, size: 32, color: Colors.blue),
      subtitle: Text('23 November 2019'),
      trailing: Icon(Ionicons.md_menu),
    );
  }

}