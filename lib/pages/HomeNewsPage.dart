import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:pdamtkr/model/NewsItem.dart';
import 'package:pdamtkr/widget/NewsCard.dart';
import 'package:pdamtkr/widget/NewsListItem.dart';

class HomeNewsPage extends  StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeNewsState();
  }

}

class _HomeNewsState extends State<HomeNewsPage> {

  double position = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: 40, left: 0, right: 0),
        child: Column(
          children: <Widget>[
            CarouselSlider(
              // height: 120,
              aspectRatio: 16/9,
              viewportFraction: 0.8,
              enlargeCenterPage: true,
              autoPlay: true,
              items: <Widget>[
                NewsCard(
                  item: NewsItem(
                    image: AssetImage("assets/contoh/news1.jpg"),
                    text: "Bupati berkunjung ke PDAM TKR"
                  )
                ),
                NewsCard(
                  item: NewsItem(
                    image: AssetImage("assets/contoh/news2.jpg"),
                    text: "Penghargaan terhadap pegawai"
                  )
                ),
                NewsCard(
                  item: NewsItem(
                    image: AssetImage("assets/contoh/news3.jpg"),
                    text: "Study Tour PDAM TKR"
                  )
                ),
                NewsCard(
                  item: NewsItem(
                    image: AssetImage("assets/contoh/news4.jpg"),
                    text: "Bantuan Air Bersih PDAM TKR"
                  )
                ),
                NewsCard(
                  item: NewsItem(
                    image: AssetImage("assets/contoh/news5.jpg"),
                    text: "Pelatihan pegawai untuk kemajuan"
                  )
                )
              ],
              onPageChanged: (pos) => {
                setState(() => {
                  position = pos.toDouble()
                })
              },
            ),
            Container(
              height: 0.5,
              color: Colors.black26,
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 12, right: 12, top: 24, bottom: 0),
            ),
            _listNews()
          ],
        ),
      ),
    );
  }

  Widget _listNews() {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          NewsListItem(),
          NewsListItem()
        ],
      ),
    );
  }

}