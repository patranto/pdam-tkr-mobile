import 'package:flutter/material.dart';
import 'package:pdamtkr/pages/TagihanPage/TagihanHistory.dart';
import 'TagihanPage/TagihanCard.dart';

class TagihanPage extends StatelessWidget {
   @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _head(),
      body: _body()
    );
  }

  Widget _head() {
    return AppBar(
      title: Text("Tagihan"),
      elevation: 0,
    );
  }

  Widget _body() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white38
      ),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 20, right: 20, top: 5),
              child: TagihanCard()
            ),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: TagihanHistory(),
            )
          ],
        ),
      ),
    );
  }

}