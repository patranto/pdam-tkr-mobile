import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:pdamtkr/model/NewsItem.dart';
import 'package:pdamtkr/widget/NewsCard.dart';

class HeaderNews extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HeadersState();
  }

}
class _HeadersState extends State<HeaderNews> {

  double position = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Informasi",
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w100,
              fontFamily: 'gnuolane',
              color: Colors.black54
            ),
          ),
          SizedBox(
            height: 10
          ),
          CarouselSlider(
            // height: 120,
            aspectRatio: 4/3,
            viewportFraction: 1.0,
            autoPlay: true,
            items: <Widget>[
              NewsCard(
                item: NewsItem(
                  image: AssetImage("assets/news1.jpeg"),
                  text: ""
                )
              ),
              NewsCard(
                item: NewsItem(
                  image: AssetImage("assets/news2.jpeg"),
                  text: ""
                )
              ),
            ],
            onPageChanged: (pos) => {
              setState(() => {
                position = pos.toDouble()
              })
            },
          ),
          SizedBox(height: 5),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              DotsIndicator(
                dotsCount: 2,
                position: position,
              )
            ],
          )
        ],
      ),
    );
  }

}