import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:pdamtkr/model/NewsItem.dart';
import 'package:pdamtkr/widget/NewsCard.dart';

class HeaderButton extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HeadersButtonState();
  }

}
class _HeadersButtonState extends State<HeaderButton> {

  double position = 0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CarouselSlider(
            // height: 120,
            aspectRatio: 18/9,
            viewportFraction: 0.8,
            // autoPlay: true,
            items: <Widget>[
              GestureDetector(
                onTap: () => {
                  Navigator.pushNamed(context, "/loket")
                },
                child: NewsCard(
                  item: NewsItem(
                    image: AssetImage("assets/contoh/news1.jpg"),
                    text: "Loket"
                  )
                ),
              ),
              NewsCard(
                item: NewsItem(
                  image: AssetImage("assets/contoh/news2.jpg"),
                  text: "Tentang Kami"
                )
              )
            ],
            onPageChanged: (pos) => {
              setState(() => {
                position = pos.toDouble()
              })
            },
          ),
          SizedBox(height: 5),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              DotsIndicator(
                dotsCount: 2,
                position: position,
              )
            ],
          )
        ],
      ),
    );
  }

}