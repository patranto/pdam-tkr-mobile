import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:pdamtkr/widget/RoundColorCard.dart';

class HeaderOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 13, right: 13),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: RoundColorCard(
                  color: Colors.red[400],
                  text: "Tagihan",
                  icon: FontAwesome.tasks,
                  round: 4.0,
                  onTap: () => {
                    Navigator.pushNamed(context, "/tagihan")
                  }
                ),
              ),
              SizedBox(width: 10),
              Column(
                children: <Widget>[
                  RoundColorCard(
                    color: Colors.green[400],
                    text: "Pemakaian",
                    icon: Ionicons.md_stats,
                    round: 3.5,
                    height: 46,
                    oneline: true,
                    onTap: () => {
                      Navigator.pushNamed(context, "/pemakaian")
                    }
                  ),
                  SizedBox(height: 8),
                  RoundColorCard(
                    color: Colors.orange,
                    text: "Pengaduan",
                    icon: Ionicons.md_contacts,
                    round: 3.5,
                    height: 46,
                    oneline: true,
                    onTap: () => {
                      Navigator.pushNamed(context, "/pengaduan")
                    }
                  )
                ],
              ),
              SizedBox(width: 8),
              Column(
                children: <Widget>[
                  RoundColorCard(
                    color: Colors.blue[400],
                    text: "Baca Meter",
                    icon: Ionicons.md_speedometer,
                    round: 3.5,
                    height: 46,
                    oneline: true,
                    onTap: () => {
                      Navigator.pushNamed(context, "/bacameter")
                    }
                  ),
                  SizedBox(height: 8),
                  RoundColorCard(
                    color: Colors.deepOrange[400],
                    text: "Pasang Baru",
                    icon: Ionicons.md_map,
                    round: 3.5,
                    height: 46,
                    oneline: true,
                    onTap: () => {
                      Navigator.pushNamed(context, "/pasangbaru")
                    }
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }

}