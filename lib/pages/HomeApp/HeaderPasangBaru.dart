import 'package:flutter/material.dart';

class HeaderPasangBaru extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return GestureDetector(
      onTap: () => {
        Navigator.pushNamed(context, '/pasangbaru')
      },
      child: Padding(
        padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Pasang Baru",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w100,
                fontFamily: 'gnuolane',
                color: Colors.black54
              ),
            ),
            SizedBox(
              height: 10
            ),
            Padding(
              padding: EdgeInsets.only(left: 5.0, right: 5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(color: Colors.black)
                        ],
                        borderRadius: BorderRadius.all(
                          Radius.circular(7)
                        ),
                        image: DecorationImage(
                          image: AssetImage('assets/img.jpeg'),
                          fit: BoxFit.cover
                        ),
                      ),
                      height: 90,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

}