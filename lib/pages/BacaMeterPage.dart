import 'package:flutter/material.dart';
import 'package:pdamtkr/pages/BacaMeterPage/BacaMeterHistory.dart';
import 'package:pdamtkr/pages/BacaMeterPage/BacaMeterPicture.dart';
import 'package:pdamtkr/pages/BacaMeterPage/TakePicture.dart';

class BacaMeterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BacaMeterState();
  }

}

class _BacaMeterState extends State<BacaMeterPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: _head(context),
        body: _body(context),
      ),
    );
  }

  Widget _head(BuildContext context) {
    return AppBar(
      bottom: TabBar(
        tabs: <Widget>[
          Tab(text: 'Ambil Gambar'),
          Tab(text: 'History')
        ],
      ),
      title: Text("Baca Meter", 
        style: TextStyle(
          color: Colors.white
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.white
      ),
      backgroundColor: Colors.blue,
    );
  }

  Widget _body(BuildContext context) {
    return TabBarView(
      children: <Widget>[
        BacaMeterPicture(),
        BacaMeterHistory()
      ],
    );
  }

}