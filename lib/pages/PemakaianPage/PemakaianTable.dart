import 'package:flutter/material.dart';

class PemakaianTable extends StatelessWidget {

  final TextStyle headerStyle = TextStyle(
    fontSize: 10,
    color: Colors.white
  );

  List<TableRow> rows = [];

  TableRow generateHeader() {
    return TableRow(
      children: [
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Container(
            color: Colors.green,
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Center(child: Text('BULAN', style: headerStyle,),
            ),),
          ),
        ),
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Container(
            color: Colors.green,
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Center(child: Text('STAND AWAL', style: headerStyle),
            ),
          ) 
          ,),
        ),
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Container(
            color: Colors.green,
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Center(child: Text('STAND AKHIR', style: headerStyle),
            ),
          ),),
        ),
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Container(
            color: Colors.green,
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Center(child: Text('Pemakaian M3', style: headerStyle),),
            )
          ),
        ),
      ]);
  }

  TableRow generateValue(String bulan, String standAwal, String standAkhir, String penggunaan) {
    return TableRow(
      children: [
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Container(
            color: Colors.green,
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Center(child: Text(bulan, style: TextStyle(
                color: Colors.white,
                fontSize: 10,
              ),),
            ),),
          ),
        ),
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Container(
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Center(child: Text(standAwal, style: TextStyle(
                color: Colors.black54,
                fontSize: 10
              )),
            ),
          ) 
          ,),
        ),
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Container(
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Center(child: Text(standAkhir, style: TextStyle(
                color: Colors.black54,
                fontSize: 10
              )),
            ),
          ),),
        ),
        TableCell(
          verticalAlignment: TableCellVerticalAlignment.middle,
          child: Container(
            child: Padding(
              padding: EdgeInsets.all(5),
              child: Center(child: Text(penggunaan, style: TextStyle(
                color: Colors.black54,
                fontSize: 10
              )),),
            )
          ),
        ),
      ]);
  }

  @override
  Widget build(BuildContext context) {

    rows.clear();

    rows.add(generateHeader());
    rows.add(generateValue("Juni", "23 M3", "29 M3", "4 M3"));
    rows.add(generateValue("Agustus", "23 M3", "29 M3", "4 M3"));
    rows.add(generateValue("September", "23 M3", "29 M3", "4 M3"));
    rows.add(generateValue("November", "23 M3", "29 M3", "4 M3"));

    // TODO: implement build
    return Table(
      border: TableBorder.all(width: 1.0, color: Colors.green),
      children: rows,
    );
  }

}