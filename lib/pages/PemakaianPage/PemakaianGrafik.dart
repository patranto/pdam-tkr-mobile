import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class PemakaianGrafik extends StatelessWidget{
  final List<charts.Series> seriesList;
  final bool animate;

  PemakaianGrafik(this.seriesList, {this.animate});

  /// Creates a [BarChart] with sample data and no transition.
  factory PemakaianGrafik.withSampleData() {
    return PemakaianGrafik(
      _createSampleData(),
      // Disable animations for image tests.
      animate: true,
    );
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: Column(
        children: <Widget>[
            Expanded(
              child: charts.BarChart(
                seriesList,
                animate: animate,
                domainAxis: charts.OrdinalAxisSpec(
                  renderSpec: charts.SmallTickRendererSpec(
                    labelStyle: charts.TextStyleSpec(
                      fontSize: 8
                    ),
                    lineStyle: charts.LineStyleSpec(
                      color: charts.MaterialPalette.green.shadeDefault
                    )
                  )
                ),
                primaryMeasureAxis: charts.NumericAxisSpec(
                  renderSpec: charts.GridlineRendererSpec(
                    labelStyle: charts.TextStyleSpec(
                      fontSize: 10
                    )
                  )
                ),
              ),
            ),
            Text(
              'M3 Kubik',
              style: TextStyle(
                fontSize: 10
              ),
            )
        ],
      ),
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<OrdinalSales, String>> _createSampleData() {
    final data = [
      new OrdinalSales('November 2019', 5),
      new OrdinalSales('Oktober 2019', 25),
      new OrdinalSales('September 2019', 10),
      new OrdinalSales('Agustus 2019', 75),
    ];

    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data,
        
      )
    ];
  }
}

/// Sample ordinal data type.
class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}