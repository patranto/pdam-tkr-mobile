import 'package:flutter/material.dart';

class HeaderPemakaian extends StatelessWidget {

  final TextStyle labelStyle = TextStyle(
    color: Colors.black54,
    fontSize: 10,
    fontWeight: FontWeight.w300,
    fontFamily: 'OpenSans'
  );

  final TextStyle valueStyle = TextStyle(
    color: Colors.black54,
    fontWeight: FontWeight.w700,
    fontFamily: 'OpenSans'
  );

  final TextStyle valueStyle2 = TextStyle(
    color: Colors.black54,
    fontSize: 12,
    fontWeight: FontWeight.w700,
    fontFamily: 'OpenSans'
  );

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.green[600],
            Colors.white30
          ],
          stops: [0.5, 0.5],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter
        )
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 15, bottom: 5, top: 0),
            child: Text('Pemakaian',
              style: TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.w700,
                fontFamily: 'OpenSans'
              ),
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Center(
                  child: Container(
                    width: double.maxFinite,
                    margin: EdgeInsets.only(left:20, top: 5, bottom: 30, right: 20),
                    padding: EdgeInsets.only(left: 12, top: 15, right: 10, bottom: 20),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black12,
                          blurRadius: 4,
                          spreadRadius: 0.2
                        )
                      ],
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Pemakaian Air Anda', 
                          style: labelStyle
                        ),
                        SizedBox(height: 3),
                        Text('KUBIKASI 84 M3', 
                          style: valueStyle,
                        ),
                        SizedBox(height: 18,),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Nomor Pelanggan',
                                    style: labelStyle,),
                                  Text(
                                    'A100000',
                                    style: valueStyle2,
                                  )   
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 30),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Periode',
                                    style: labelStyle,),
                                  Text(
                                    'November 2019',
                                    style: valueStyle2,
                                  )
                                ],
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

}