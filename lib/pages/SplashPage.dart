import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Future.delayed(const Duration(milliseconds: 3000), () {
      Navigator.pushReplacementNamed(context, '/welcome');
    });

    return Scaffold(
      appBar: null,
      body: Container(
        color: Colors.blue[600],
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height:10),
              Image(
                image: AssetImage('assets/splash.png'),
                width: 200,
              ),
              SizedBox(height: 20),
              Text('Perusahaan Daerah Air Minum',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans'
                ), 
              ),
              Text('TIRTA KERTA RAHARJA',
                style: TextStyle(
                  color: Colors.deepPurple,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans'
                ),
              ),
              Text('KABUPATEN TANGERANG',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans'
                ),
              ),
            ],
          )
        ),
      )
    );
  }

}