import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class LokasiLoketPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: _head(context),
      body: _body(context),
    );
  }

  Widget _head(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.deepOrange[400],
      title: Text("Lokasi Loket", 
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.white
      ),
    );
  }

  Widget _body(BuildContext context) {
    return ListView(
      children: <Widget>[

        createTile("Loket 1", "Jalan Bandung", Ionicons.ios_pin),
        createTile("Loket 2", "Jalan Depok", Ionicons.ios_pin),
        createTile("Loket 3", "Jalan Tangerang", Ionicons.ios_pin),
        createTile("Loket 4", "Jalan Cirebon", Ionicons.ios_pin),
        createTile("Loket 5", "Jalan Jakarta", Ionicons.ios_pin),


      ],
    );
  }

  ListTile createTile(String title, String alamat, IconData icon) {
    return ListTile(
      title: Text(title),
      subtitle: Text(alamat),
      trailing: Icon(icon, color: Colors.deepOrange[400],),
    );
  }

}