import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';

class WelcomeSliderPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _WelcomeSliderState();
  }
}

class _WelcomeSliderState extends State<WelcomeSliderPage>{

  double position = 0;

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: null,
      body: Container(
        color: Colors.blue,
        padding: EdgeInsets.only(top: 40),
        child: Column(
          children: <Widget>[
            Expanded(
              child: PageView(
                children: <Widget>[
                  _pageOne(context),
                  _pageTwo(context),
                  _pageThree(context),
                  _pageFour(context)
                ],
                onPageChanged: (pos) => {
                  setState(() => {
                    this.position = pos.toDouble()
                  })
                },
              ),
            ),
            DotsIndicator(
              dotsCount: 4,
              position: position,
              decorator: DotsDecorator(
                color: Colors.white,
                activeColor: Colors.blue[800]
              ),
            ),
            SizedBox(
              height: 80
            )
          ],
        ),
      )
    );
  }

  Widget _pageOne(BuildContext context) {
    return Container(
      color: Colors.blue[500],
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage('assets/intro/page1.png'),
              width: 200
            ),
            SizedBox(height: 40),
            Container(
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Text('Selamat Datang di PDAM TKR Mobile App', 
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans',
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(height: 10),
            Container(
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 10, right: 10),
              child: Text('Perkenalkan Tampilan baru TKR Mobile App yang memberikan kemudahan dalam Genggaman Smart Phone Anda', 
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'OpenSans',
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  Widget _pageTwo(BuildContext context) {
    return Container(
      color: Colors.blue[500],
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage('assets/intro/page2.png'),
              width: 200
            ),
            SizedBox(height: 40),
            Container(
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Text('Pasang Baru Online', 
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans',
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(height: 10),
            Container(
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 10, right: 10),
              child: Text('Pasang Baru kini sudah bisa melalui Mobile App PDAM TKR Daftar dan nikmati dalam kemudahan untuk layanan pasang baru', 
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'OpenSans',
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  Widget _pageThree(BuildContext context) {
    return Container(
      color: Colors.blue[500],
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage('assets/intro/page3.png'),
              width: 200
            ),
            SizedBox(height: 40),
            Container(
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Text('Informasi Seputar PDAM TKR', 
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans',
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(height: 10),
            Container(
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 10, right: 10),
              child: Text('Informasi Gangguan Layanan dan lain nya sudah dapat anda nikmati dari MyTkr didalam Smart phone anda', 
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'OpenSans',
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  Widget _pageFour(BuildContext context) {
    return Container(
      color: Colors.blue[500],
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage('assets/intro/page4.png'),
              width: 200
            ),
            SizedBox(height: 40),
            Container(
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Text('Kendali Dalam Genggaman', 
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans',
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(height: 10),
            Container(
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 10, right: 10),
              child: Text('Rasakan semua kendali layanan aplikasi Mobile App PDAM TKR dalam genggaman Anda', 
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'OpenSans',
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(height: 10),
            RaisedButton(
              onPressed: () => {
                Navigator.pushNamedAndRemoveUntil(context, '/homenotlogin', (Route<dynamic> route) => false)
              },
              color: Colors.green,
              child: Text('Coba Sekarang', 
                style: TextStyle(
                  color: Colors.white
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}