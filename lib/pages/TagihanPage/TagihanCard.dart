import 'package:flutter/material.dart';

class TagihanCard extends StatelessWidget {

  final Color whiteColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(0),
            child: Container(
              child: Padding(
                padding: EdgeInsets.all(12),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "PERUMDAM TIRTA KERTARAHARJA",
                      style: TextStyle(
                        color: whiteColor,
                        fontSize: 12,
                        fontWeight: FontWeight.w700,
                        fontFamily: 'OpenSans'
                      ),
                    ),
                    SizedBox(height: 12),
                    Text(
                      "Tagihan A100000000",
                      style: TextStyle(
                        color: whiteColor,
                        fontWeight: FontWeight.w300,
                        fontFamily: 'OpenSans',
                        fontSize: 10
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(left: 1),
                            child: Text(
                              "November, 2020",
                              style: TextStyle(
                                color: whiteColor,
                                fontFamily: 'OpenSans',
                                fontSize: 14,
                                fontWeight: FontWeight.normal
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 7),
                          child: Text(
                            "Rp. 95.000.00,-",
                            style: TextStyle(
                              color: whiteColor,
                              fontFamily: 'OpenSans',
                              fontSize: 14,
                              fontWeight: FontWeight.w700
                            )
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 20,),
                    RaisedButton(
                      padding: EdgeInsets.all(0.0),
                      child: Container(
                        child: Text(
                          "BAYAR",
                          style: TextStyle(
                            color: whiteColor,
                            fontSize: 12
                          )
                        ),
                      ),
                      onPressed: null,
                    )
                  ],
                ),
              ),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomRight,
                  end: Alignment.topLeft,
                  stops: [0.1, 0.5, 0.7],
                  colors: [
                    Colors.blue[300],
                    Colors.blue[400],
                    Colors.blue[500]
                  ]
                ),
                borderRadius: BorderRadius.all(Radius.circular(4)),
                border: Border.all(
                  color: Colors.blue[300],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

}