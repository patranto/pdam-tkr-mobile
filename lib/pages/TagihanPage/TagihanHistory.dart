import 'package:flutter/material.dart';
import 'package:pdamtkr/model/TagihanItem.dart';

class TagihanHistory extends StatelessWidget {

  final List<TagihanItem> items = [
    TagihanItem(
      text: "Desember 2019",
      islate: false,
      date: "02 Desember 2019",
    ),
    TagihanItem(
      text: "November 2019",
      islate: true,
      date: "23 November 2019"
    ),
    TagihanItem(
      text: "Oktober 2019",
      islate: false,
      date: "01 Oktober 2019",
    ),
    TagihanItem(
      text: "September 2019",
      islate: false,
      date: "06 September 2019",
    ),
    TagihanItem(
      text: "Agustus 2019",
      islate: false,
      date: "01 Agustus 2019",
    )
  ]; 

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(7)),
        border: Border.all(
          color: Colors.white30
        )
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "History A100000000",
            style: TextStyle(
              fontFamily: 'gnuolane',
              fontSize: 16,
              color: Colors.black54
            ),
          ),
          SizedBox(height: 8),
          Container(
            height: 1,
            width: double.maxFinite,
            color: Colors.grey[300]
          ),
          Container(
            child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: items,
            ),
          )
        ],
      ),
    );
  }

}