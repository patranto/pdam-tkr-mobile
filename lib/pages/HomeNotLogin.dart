import 'package:flutter/material.dart';
import 'package:pdamtkr/pages/HomeApp/HeaderButton.dart';

import 'HomeApp/HeaderNews.dart';


class HomeNotLogin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    
    return _HomeNotLoginState();
  }

}

class _HomeNotLoginState extends State<HomeNotLogin> {

  final double circular = 4.0;

  @override
  void initState() {
    super.initState();
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: _body(context),
    );
  }

 

  Widget _body(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(),
      margin: EdgeInsets.only(top: 24),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _cardWelcome(),
              SizedBox(height: 20),
              // HeaderPasangBaru(),
              HeaderNews(),
              HeaderButton(),
              SizedBox(height: 30,)
            ],
          )
        )
      )
    );
  }

  Widget _cardWelcome() {
    return Container(
        width: double.maxFinite,
        margin: EdgeInsets.only(left: 20, right: 20),
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: Colors.blue,
          gradient: LinearGradient(
            begin: Alignment.bottomRight,
            end: Alignment.topLeft,
            stops: [0.1, 0.5, 0.7],
            colors: [
              Colors.blue[300],
              Colors.blue[400],
              Colors.blue[500]
            ]
          ),
          borderRadius: BorderRadius.all(Radius.circular(4)),
          border: Border.all(
            color: Colors.blue[300],
          )
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Selamat datang di MyTKR', 
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontFamily: 'OpenSans',

              ),
            ),
            Text('Silahkan login atau registrasi akun Anda untuk menikmati layanan MyTkr', 
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.normal,
                fontSize: 11,
                fontFamily: 'OpenSans',
              ),
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  color: Colors.red,
                  child: Text('LOGIN',
                    style: TextStyle(
                      color: Colors.white
                    ),
                  ),
                  onPressed: () => {
                    Navigator.pushNamed(context, '/login')
                  },
                ),
                SizedBox(width: 60,),
                RaisedButton(
                  color: Colors.green,
                  child: Text('REGISTER', 
                    style: TextStyle(
                      color: Colors.white
                    ),
                  ),
                  onPressed: () => {

                  },
                )
              ],
            )
          ],
        ),
      );
  }
}