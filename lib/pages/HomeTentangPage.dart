import 'package:flutter/material.dart';

class HomeTentangPage extends  StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _HomeTentangState();
  }

}

class _HomeTentangState extends State<HomeTentangPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 30, left: 20, right: 20),
      child: Text('Tentang Kami'),
    );
  }

}