import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:pdamtkr/pages/HomeApp/HeaderButton.dart';
import 'package:pdamtkr/pages/HomeApp/HeaderOne.dart';
import 'package:pdamtkr/pages/HomeNewsPage.dart';
import 'package:pdamtkr/pages/HomePengaduanPage.dart';
import 'package:pdamtkr/pages/HomeTentangPage.dart';
import 'package:pdamtkr/pages/ProfileSettingPage.dart';
import 'package:pdamtkr/pages/TagihanPage/TagihanCard.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'HomeApp/HeaderNews.dart';


class HomeApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    
    return _HomeState();
  }

}

class _HomeState extends State<HomeApp> {

  final double circular = 4.0;
  SharedPreferences prefs;
  bool isLogin = false;

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((prefs) => {
      setState(() => {
        isLogin = prefs.getBool('is_login') ?? false
      })
    });
    
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      initialIndex: 2,
      child: Scaffold(
        appBar: null,
        body: TabBarView(
          children: <Widget>[
            HomeNewsPage(),
            HomePengaduanPage(),
            _body(context),
            HomeTentangPage(),
            Container(
              child: ProfileSettingPage(),
            ),
          ],
        ),
        bottomNavigationBar: _bottomBar(context),
      )
    );
  }

  Widget _headerWelcome() {
    return Padding(
      padding: EdgeInsets.only(left: 20.0, top: 20, right: 7.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: Text(
                "Selamat Siang",
                style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.normal,
                  fontFamily: 'gnuolane',
                  color: Colors.black87
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 5),
            child: IconButton(
              onPressed: () => {
                Navigator.pushNamed(context, '/notification')
              },
              icon: Icon(
                Icons.notification_important,
                color: Colors.black38,
                size: 20,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 5),
            child: IconButton(
              onPressed: () => {
                // Navigator.pushNamed(context, '/notification')
              },
              icon: Icon(
                Ionicons.md_mail,
                color: Colors.black38,
                size: 20,
              ),
            ),
          )
        ],
      )
    );
  }

  Widget _body(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(),
      margin: EdgeInsets.only(top: 24),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              isLogin ? _headerWelcome() : SizedBox(),
              isLogin ? _tagihanCard(): _cardWelcome(),
              SizedBox(height: 20),
              isLogin ? HeaderOne(): SizedBox() ,
              // HeaderPasangBaru(),
              HeaderNews(),
              HeaderButton(),
              SizedBox(height: 30,)
            ],
          )
        )
      )
    );
  }

  Widget _cardWelcome() {
    return Container(
        width: double.maxFinite,
        margin: EdgeInsets.only(left: 20, right: 20),
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: Colors.blue,
          gradient: LinearGradient(
            begin: Alignment.bottomRight,
            end: Alignment.topLeft,
            stops: [0.1, 0.5, 0.7],
            colors: [
              Colors.blue[300],
              Colors.blue[400],
              Colors.blue[500]
            ]
          ),
          borderRadius: BorderRadius.all(Radius.circular(4)),
          border: Border.all(
            color: Colors.blue[300],
          )
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Selamat datang di MyTKR', 
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontFamily: 'OpenSans',

              ),
            ),
            Text('Silahkan login atau registrasi akun Anda untuk menikmati layanan MyTkr', 
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.normal,
                fontSize: 11,
                fontFamily: 'OpenSans',
              ),
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  color: Colors.red,
                  child: Text('LOGIN',
                    style: TextStyle(
                      color: Colors.white
                    ),
                  ),
                  onPressed: () => {
                    Navigator.pushNamed(context, '/login')
                  },
                ),
                SizedBox(width: 60,),
                RaisedButton(
                  color: Colors.green,
                  child: Text('REGISTER', 
                    style: TextStyle(
                      color: Colors.white
                    ),
                  ),
                  onPressed: () => {

                  },
                )
              ],
            )
          ],
        ),
      );
  }

  Widget _tagihanCard() {
    return Padding(
      padding: EdgeInsets.only(left: 20, right:20),
      child: TagihanCard()
    );
  }

  Widget _bottomBar(BuildContext context) {
    return TabBar(
      tabs: <Widget>[
        Tab(
          icon: Icon(FontAwesome5.newspaper)
        ),
        Tab(
          icon: Icon(Ionicons.ios_people)
        ),
        Tab(
          icon: Icon(Ionicons.ios_home)
        ),
        Tab(
          icon: Icon(Ionicons.ios_information_circle)
        ),
        Tab(
          icon: Icon(Ionicons.ios_settings)
        ),
      ],
      labelColor: Colors.blue[400],
      unselectedLabelColor: Colors.black38,
    );
  }
}