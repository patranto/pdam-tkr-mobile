import 'package:flutter/material.dart';
import 'package:pdamtkr/pages/PemakaianPage/HeaderPemakaian.dart';
import 'package:pdamtkr/pages/PemakaianPage/PemakaianGrafik.dart';
import 'package:pdamtkr/pages/PemakaianPage/PemakaianTable.dart';

class PemakaianPage extends StatefulWidget {
  @override
  _PemakaianState createState() => _PemakaianState();
}

class _PemakaianState extends State<PemakaianPage> with TickerProviderStateMixin {

  TabController _nestedController;

  @override
  void initState() {
    super.initState();

    _nestedController = new TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _nestedController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: _head(context),
      body: _body(context),
    );
  }

  Widget _head(BuildContext context) {
    return AppBar(
      title: Text("", 
      style: TextStyle(
          color: Colors.white
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.white
      ),
      elevation: 0,
      backgroundColor: Colors.green[600],
    );
  }

  Widget _body(BuildContext context) {
    double screenHeight = 400;

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          HeaderPemakaian(),
          Container(
            child: TabBar(
              tabs: <Widget>[
                Tab(text: 'Grafik'),
                Tab(text: 'Table')
              ],
              labelColor: Colors.green[300],
              indicatorColor: Colors.green[300],
              controller: _nestedController,
              // isScrollable: true,
            ),
          ),
          Container(
            height: screenHeight,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: Colors.green[100]
              )
            ),
            child: TabBarView(
              controller: _nestedController,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 30, right:20, top: 30, bottom: 30),
                  child: PemakaianGrafik.withSampleData(),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 30, right:20, top: 30, bottom: 30),
                  child: PemakaianTable(),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

}