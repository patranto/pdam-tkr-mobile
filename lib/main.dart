import 'package:flutter/material.dart';
import 'package:pdamtkr/pages/BacaMeterPage.dart';
import 'package:pdamtkr/pages/HomeApp.dart';
import 'package:pdamtkr/pages/HomeNotLogin.dart';
import 'package:pdamtkr/pages/LoginPage.dart';
import 'package:pdamtkr/pages/LokasiLoketPage.dart';
import 'package:pdamtkr/pages/NotificationPage.dart';
import 'package:pdamtkr/pages/PasangBaruPage.dart';
import 'package:pdamtkr/pages/PemakaianPage.dart';
import 'package:pdamtkr/pages/PengaduanPage.dart';
import 'package:pdamtkr/pages/SplashPage.dart';
import 'package:pdamtkr/pages/TagihanPage.dart';
import 'package:pdamtkr/pages/WelcomeSliderPage.dart';

void main() => runApp(MainApp());

class MainApp extends StatelessWidget {

  final iconTextColorTitle = Colors.black;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: SplashPage(),
        theme: _theme(),
        title: "Route",
        routes: <String, WidgetBuilder> {
          '/welcome': (BuildContext context) => WelcomeSliderPage(),
          '/login': (BuildContext context) => LoginPage(),
          '/homenotlogin': (BuildContext context) => HomeNotLogin(),
          '/home': (BuildContext context) => HomeApp(),
          '/tagihan': (BuildContext context) => TagihanPage(),
          '/notification': (BuildContext context) => NotificationPage(),
          '/pengaduan': (BuildContext context) => PengaduanPage(),
          '/pemakaian': (BuildContext context) => PemakaianPage(),
          '/pasangbaru': (BuildContext context) => PasangBaruPage(),
          '/loket': (BuildContext context) => LokasiLoketPage(),
          '/bacameter': (BuildContext context) => BacaMeterPage()
        }
    );
  }

  ThemeData _theme() {
    return new ThemeData(
      appBarTheme: AppBarTheme(
        brightness: Brightness.dark,
        color: Colors.white,
        iconTheme: IconThemeData(color: iconTextColorTitle),
        actionsIconTheme: IconThemeData(color: iconTextColorTitle),
        textTheme: TextTheme(title: TextStyle(
          color: iconTextColorTitle,
          fontWeight: FontWeight.bold,
          fontSize: 16
        ))
      )
    );
  }

}


