import 'package:flutter/material.dart';

class RoundColorCard extends StatelessWidget {
  final double round;
  final Color color;
  final String text;
  final IconData icon;
  final double width;
  final double height;
  final bool oneline;
  Function onTap = () => {};

  RoundColorCard({
    this.text, 
    this.color, 
    this.round, 
    this.icon, 
    this.width, 
    this.height, 
    this.oneline,
    this.onTap
  });

  @override
  Widget build(BuildContext context) {
    
    return GestureDetector(
      onTap: onTap,
      child: Container(
        alignment: Alignment(0.0, 0.0),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(round),
            topRight: Radius.circular(round),
            bottomLeft: Radius.circular(round),
            bottomRight: Radius.circular(round)
          )
        ),
        child: oneline != null ? getOnelineChild() : getChild(),
        width: width == null ? 100: width,
        height: height == null ? 100: height
      )
    );
  }

  Widget getOnelineChild() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
          Icon(
            icon,
            color: Colors.white,
            size: 14,
          ),
          SizedBox(width: 4),
          Padding(
            padding: EdgeInsets.only(top: 2),
            child: Text(
              text,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 11,
                  fontWeight: FontWeight.bold
              )
            )
          )
      ],
    );
  }

  Widget getChild() {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 3),
            child: Icon(
              icon,
              color: Colors.white,
              size: 22,
            ),
          ),
          SizedBox(height: 1),
          Text(
            text,
            style: TextStyle(
                color: Colors.white,
                fontSize: 13,
                fontWeight: FontWeight.bold
            )
          ),
        ],
      );
  }

}