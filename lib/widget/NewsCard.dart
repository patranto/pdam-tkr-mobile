import 'package:flutter/material.dart';
import 'package:pdamtkr/model/NewsItem.dart';

class NewsCard extends StatelessWidget {
  final NewsItem item;

  NewsCard({
    this.item
  });
  
  @override
  Widget build(BuildContext context) {
    
    return Container(
            width: double.maxFinite,
            margin: EdgeInsets.symmetric(horizontal: 5.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(4)),
              border: Border.all(
                color: Colors.grey[200]
              )
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(4), topRight: Radius.circular(4)),
                      border: Border.all(
                        color: Colors.grey[200],
                        width: 0.5
                      ),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: item.image
                      )
                    ),
                  ),
                ),
                SizedBox(height: 4),
                Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Text (
                    item.text,
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.grey[700]
                    ),
                  ),
                ),
                SizedBox(height: 6)
              ],
            ),
          );;
  }

}