import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_icons/flutter_icons.dart';

class TagihanItem extends StatelessWidget {
  final String text;
  final bool islate;
  final String date;

  TagihanItem({
    this.text,
    this.islate,
    this.date
  });

  @override
  Widget build(BuildContext context) {

    final icon = Icon(
      Ionicons.ios_water, 
      color: Colors.blue[400],
      size: 29,
    );

    String tunai = 'LUNAS';

    Color color = Colors.green[300];

    if (islate) {
      color = Colors.red[300];
      tunai = 'TELAT';
    }

    return ListTile(
      leading: icon,
      title: Text(
        text,
        style: TextStyle(
          fontSize: 13,
          fontWeight: FontWeight.w700,
          fontFamily: 'OpenSans',
          color: color,
        ),
      ),
      subtitle: Text(
        date,
        style: TextStyle(
          fontSize: 10.5,
          fontWeight: FontWeight.w300,
          fontFamily: 'OpenSans'
        )
      ),
      trailing: Container(
        width: 70,
        height: 30,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(3))
        ),
        child: Padding(
          padding: EdgeInsets.all(5),
          child: Text(
            tunai,
            style: TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.w700,
              fontFamily: 'OpenSans',
              color: Colors.white,
            ),
          )
        )
      ),
    );
  }
}