import 'package:flutter/widgets.dart';

class NewsItem {
  final String text;
  final ImageProvider image;

  NewsItem({
    this.text,
    this.image
  });
}